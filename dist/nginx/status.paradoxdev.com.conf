upstream php-fpm-sock {
    server unix:/var/run/php-fpm/linux-dash.sock;
}

server {
    server_name     status.paradoxdev.com
                    status.paradoxdev.local;
    root            /usr/share/webapps/linux-dash/app;
    index           index.html index.php;
    access_log      /var/log/nginx/linux-dash-access.log;
    error_log       /var/log/nginx/linux-dash-error.log;

    auth_basic            'access restricted';
    auth_basic_user_file  /etc/webapps/linux-dash/auth;

    # Cache static files for as long as possible
    location ~* \.(?:xml|ogg|mp3|mp4|ogv|svg|svgz|eot|otf|woff|ttf|css|js|jpg|jpeg|gif|png|ico)$ {
        try_files $uri =404;
        expires max;
        access_log off;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }

    # Pass PHP requests on to PHP-FPM using sockets
    location ~ \.php(/|$) {
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        fastcgi_pass php-fpm-sock;
        if (!-f $document_root$fastcgi_script_name) {
            return 404;
        }
        try_files $uri $uri/ /index.php?$args;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
