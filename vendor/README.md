# Vendor Code

## [linux-dash](https://github.com/tariqbuilds/linux-dash)

### Dependencies

*Note:* all dependencies listed according to ArchLinux package names.

__Runtime:__

- `php`
- `nginx`

### Setup

0. Install dependencies listed above
1. Install this project:

```bash
make install # install the entire server-status project
# or:
make install-linux-dash # just install linux-dash
```

2. Configure PHP by removing the following functions from the `disable_functions`
  list in `/etc/php/php.ini`:

  - `exec`
  - `shell_exec`
  - `escapeshellarg`

3. Enable/start php-fpm and nginx:

```bash
systemctl enable --now php-fpm.service nginx.service
```
