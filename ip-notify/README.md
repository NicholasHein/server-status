# ip-notify

## Dependencies

*Note:* all dependencies listed according to ArchLinux package names.

__Build:__

- `make`
- `coreutils`
- `systemd`
- `shadow`

__Runtime:__

- `bash`
- `util-linux`
- `sed`
- `curl`
- `postfix`

## Setup

0. Install dependencies listed above
1. Install this project:

```bash
make install # install the entire server-status project
# or:
make install-ip-notify # just install ip-notify
```

2. Enable/start service:

```bash
systemctl enable --now ip-notify
```
