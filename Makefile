#! /usr/bin/env make -f

SHELL = /bin/sh

INSTALL = /usr/bin/install
INSTALL_FILES = ${INSTALL} -vDm 644
COPY = /usr/bin/cp
COPY_FILES = ${COPY} -vf --preserve=mode

DESTDIR ?=

prefix = /usr
sysconfdir = /etc
bindir = ${prefix}/bin
libdir = ${prefix}/lib
datarootdir = ${prefix}/share
statedir = /var/lib

.PHONY: help

help:
	@echo 'TARGETS:'
	@echo '  help __________________ show this help message'
	@echo '  install _______________ install all projects'
	@echo '  install-ip-notify _____ install the "ip-notify" project'
	@echo '  install-linux-dash ____ install the "linux-dash" project'
	@echo '  uninstall _____________ uninstall all projects'
	@echo '  uninstall-ip-notify ___ uninstall the "ip-notify" project'
	@echo '  uninstall-linux-dash __ uninstall the "linux-dash" project'
	@echo ''
	@echo 'OPTIONS:'
	@echo '  DESTDIR _______________ directory for staged installs [default: (none)]'
	@echo '  prefix ________________ install prefix [default: "/usr"] '
	@echo '  sysconfdir ____________ system configuration prefix [default: "/etc"]'
	@echo '  bindir ________________ executable binary directory [default: "$${prefix}/bin"]'
	@echo '  libdir ________________ static data directory prefix [default: "$${prefix}/lib"]'
	@echo '  datarootdir ___________ read-only program data prefix [default: "$${prefix}/share"]'
	@echo '  statedir ______________ runtime state directory prefix [default: "/var/lib"]'

.PHONY: install uninstall

install: install-ip-notify install-linux-dash
uninstall: uninstall-ip-notify uninstall-linux-dash

.PHONY: install-ip-notify uninstall-ip-notify

install-ip-notify:
	@echo "INSTALLING: ip-notify"
	@echo 'installing executable'
	${INSTALL} -Dt ${DESTDIR}${bindir}/ \
		ip-notify/ip-notify

	@echo 'installing default configuration file'
	${INSTALL_FILES} -T dist/ip-notify.conf \
		${DESTDIR}${sysconfdir}/ip-notify/config.conf
	@echo 'installing systemd service'
	${INSTALL_FILES} -t ${DESTDIR}${libdir}/systemd/system/ \
		dist/systemd/ip-notify.service \
		dist/systemd/ip-notify.timer
	@echo 'allocating system user'
	${INSTALL_FILES} -T dist/systemd/ipnotify.sysusers \
		${DESTDIR}${libdir}/sysusers.d/ipnotify.conf

uninstall-ip-notify:
	@echo "UNINSTALLING: ip-notify"
	@echo 'removing installed files'
	rm -rf \
		${DESTDIR}${bindir}/ip-notify \
		${DESTDIR}${libdir}/systemd/system/ip-notify.service \
		${DESTDIR}${libdir}/systemd/system/ip-notify.timer \
		${DESTDIR}${libdir}/sysusers.d/ipnotify.conf \
		${DESTDIR}${statedir}/ip-notify/

.PHONY: install-linux-dash uninstall-linux-dash

install-linux-dash:
	@echo "INSTALLING: linux-dash"
	@echo 'installing application data'
	${INSTALL} -d ${DESTDIR}${datarootdir}/webapps/linux-dash
	${COPY_FILES} -rt ${DESTDIR}${datarootdir}/webapps/linux-dash/ \
		vendor/linux-dash/*
	@echo 'installing nginx configuration'
	${INSTALL_FILES} -t ${DESTDIR}${sysconfdir}/nginx/sites-available/ \
		dist/nginx/status.paradoxdev.com.conf
	@echo 'installing PHP configuration'
	${INSTALL_FILES} -t ${DESTDIR}${sysconfdir}/php/php-fpm.d/ \
		dist/php-fpm/linux-dash.conf
	@echo 'allocating system user'
	${INSTALL_FILES} -T dist/systemd/linuxdash.sysusers \
		${DESTDIR}${libdir}/sysusers.d/linuxdash.conf
	@echo 'configuring tmpfiles'
	${INSTALL_FILES} -T dist/systemd/linuxdash.tmpfiles \
		${DESTDIR}${libdir}/tmpfiles.d/linuxdash.conf

uninstall-linux-dash:
	@echo "UNINSTALLING: linux-dash"
	@echo 'removing installed files'
	rm -rf \
		${DESTDIR}${datarootdir}/webapps/linux-dash \
		${DESTDIR}${sysconfdir}/nginx/sites-enabled/status.paradoxdev.com.conf \
		${DESTDIR}${sysconfdir}/nginx/sites-available/status.paradoxdev.com.conf \
		${DESTDIR}${sysconfdir}/php/php-fpm.d/linux-dash.conf \
		${DESTDIR}${libdir}/tmpfiles.d/linuxdash.conf
